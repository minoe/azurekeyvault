# README #
This creates a template spec in the DevelopmentSupport Resource Group.

That spec can then be referenced by name and version when building new infrastructure.


# EXAMPLE #

```code

  $templateSpecId = (Get-AzTemplateSpec -Name clientKeyVault -Version 0.0.1).Id

  New-AzResourceGroupDeployment -ResourceGroup 'sandbox-qa' `
  -Name keyVault `
  -TemplateSpecId $templateSpecId

```
